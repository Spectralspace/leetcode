# Great Migration
![](https://media.gadventures.com/media-server/cache/19/56/19567a3711d9810ad82a42fa2d88e794.jpg)

## 2019
### Week 49
* Medium: [19. Remove Nth Node From End of List](19.md)
* Medium: [289. Game of Life](289.md)
* Easy: [198. House Robber](198.md)
* Medium: [213. House Robber II](213.md)
* Medium: [129. Sum Root to Leaf Numbers](129.md)
* Medium: [337. House Robber III](337.md)






